# architecture

```mermaid
graph TD;
SBC_PRINCIPAL-->SBC_SUPPORT
SBC_PRINCIPAL-->SBC_SERVICES
SBC_SUPPORT-->SBC_SERVICES
```

### SBC_PRINCIPAL

C'est lui qui gere tt ce qui est multimedia. Il sera branché à l'écran/clavier/souris. Il permettra également de gere tt les autres SBC.
Il faut qu'il soit puissant.

### SBC_SUPPORT

Optionnel il permettra de gerer ce qui est gesture, Assistance vocale, disques reseaux, services reseaux si besoins, les sauvegardes etc...

L'ideal est d'avoir un disque partagé en réseau par le SBC_SUPPORT et de le redonder(deux disques deux SBC_SUPPORT).

### SBC_SERVICES

Ce sont des SBC qui contiendront des applications peut animées ou non graphique afin de soulager le SBC principal.

## fonctionnement globale

* les noeuds ont des roles predefinis.
* la compilation est distribuée sur tt les noeuds
* le noeud principale gere l'affichage et les entrées sorties mais tt les GPIO restent utilisables.
* un wrapper simple permettra de commander les noeuds

### cas d'utilisations

#### dev IOT

en dev IOT on a besoin de ports GPIO, d'un acces reseau, eventuellement d'une gateway, d'une chaine de compilation, d'un IDE, d'un navigateur WEB.

Tout peut aller sur les noeuds de services sauf le navigateur web. Même si cela fonctionne sur un noeud de service c'est plus confortable sur le noeud principal.

#### dev WEB/services/systeme

On a besoin d'à peu pres la même chose, mais avec parfois des applications graphiques supplementaires. Ce qui est graphique sera sur le noeud principal.

#### integration continue

Quelque soit le type de dev on peut ajouter un CD/CI tel que jenkins/gitlab etc... sur les SBC_SERVICES

#### mail/partage de fichiers/WIKI/torrent/webservices

Tout cela va évidement dans la couche services.

### avantages

* maintenance : on peut maintenir le serveur web sans impacter le serveur de torrent ou le mail.
* faible coups : Pour une supermachine il faut 5SBC à 50€ + 2DD à 50€ + environ 50€ pour le reste(boitier/cable/dissipateur de chaleur/routeur). on trouve des PC à ce prix mais ils n'offrent pas de possibilités équivalente et c'est du closesource/lowcost.
* faible consomation : malgré le fait que 5 miniPC tournent nous sommes à moins de 70W(a fond).
* libre : Le materiel est ouvert, on sait ce qu'on utilise. La partie logiciel est 100% libre.

### inconvenient

* reste imaginaire : toute les briques existe mais le produit finit n'existe pas.
* DIY : il faudra le monter soit même.
* support : il existe pour chaque brique mais pas pour l'ensemble.

## STEP 2

Si l'on reussi a mettre en place cette architecture, j'aimerais par la suite ajouter une couche de virtualisation. Cela faciliterait l'installation et la maintenance.
