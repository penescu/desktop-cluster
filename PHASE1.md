# PHASE1 - Le poc

Je souhaite voir ce que donne un cluster de ce type avant d'investire dans du matériel. A la bricolotech on a du matos de recup...Parfait pour faire un cluster de ce type!

## de quoi ai-je besoin

2 machines gnu/linux equipe de port ETH + un cable ETH + une connection internet + un serveur NFS serveur + NFS client + MPICH2

ici ce qu'il faut pour installer i3wm : https://github.com/apprensemble/base

Je ferais un truc correct un jour...

## Installation GNU/Linux

J'ai installé une devuan sur une machine et une lubuntu+i3wm+tmux sur une autre.

## partage de connexion internet

Nous n'avons pas encore de connexion, j'ai donc utilisé mon tel portable sur l'une des machines et ai partagé la connexion a l'aide de la doc debian.

https://wiki.debian.org/fr/BridgeNetworkConnections

## installation du cluster

http://mpitutorial.com/tutorials/running-an-mpi-cluster-within-a-lan/
