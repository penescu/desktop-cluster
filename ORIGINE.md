# D'où vient ce projet


## L'évolution des PC

D'une longue réflexion autour de l'évolution de la relation entre les utilisateurs et leur matériel. Alors que le logiciel libre fait sa place petit à petit, le matériel semble nous échapper depuis la venue des smartphones. En effet il semble acceptable de ne pas avoir la possibilité d'installer l'os que l'on souhaite sur un smartphone ni même les logiciels. Il semble également acceptable qu'il soit obsolète au bout d'un an voir moins. 
Les smartphones sont des boites noires que tout le monde ou presque utilise. Les constructeurs semblent faire de même avec les PC. Le secure boot empeche d'installer un OS non signé par microsoft et décourage ainsi certains utilisateurs à passer à un gnu/linux ou BSD. C'est désactivable me direz vous? Et bien pas toujours. Certains constructeurs ne prennent pas la peine d'offrir la possibilité de le désactiver***(je mettrais un article sur le sujet)***. 
Ensuite avec l'IME/AMT intel décide d'ajouter une boite noire à nos PC(depuis 2008). AMD a fait de même à partir de 2013***ajouter un article la aussi***.
Cela dérange un peu, choque parfois mais il ne s'agit pas d'une option. Personnellement, je pense que l'on mérite d'avoir le choix.

## L'IOT

C'est un peu la même chose pour l'IOT mais comme nous en sommes au début et que beaucoup de gens se passionnent pour ce domaine. Les choses ont évoluées différemment.
ARDUINO est une plateforme opensource, et c'est la plus réputé. L'ESP8266 son plus grand concurrent est libre aussi(sauf la parti wifi).
Le domaine reste assez simple pour que ceux qui s'y intéresse comprenne l'intérêt de l'open source. Cela m'a permis d'entrevoir une chose. Le matériel libre comme le logiciel libre n'intéressent pas beaucoup car cela nous dépasse. Même si le libre offre la possibilité de voir comment c'est fait, de modifier et de distribuer, c'est bien trop complexe pour le commun des mortels. Alors on ferme les yeux. L'IOT c'est beaucoup moins complexe et j'ai l'impression que cela change un peu la donne.

## Quel rapport avec ce projet?

Voici ce qu'on fait de mieux en PC libre :

[librem15](https://www.crowdsupply.com/purism/librem-15)
[vikingsD16](https://store.vikings.net/libre-friendly-hardware/vikings-d16-workstation)
[recyclage](https://libreboot.org/docs/hardware/#list-of-supported-hardware)

C'est très couteux / très vieux ou très compliqué. A noté que le librem-15 est marqué comme presque libre au niveau du bios. J'avais vu un doc d'analyse du point de vu matériel et là tt un monde non libre apparait à cause du fait que l'on utilise un processeur intel. Ce petit carré bios n'est pas anodin. De mon point de vue librem15 est moins libre que certaines boards.

L'IOT et l'embarqué nous offre un retour aux années ou l'informatique grand publique était simple(les années 80). Contrairement au monde PC il existe des plateformes libres avec des pilotes libres capables de fonctionner sur un OS libre et sans hack. Malheureusement au niveau performance, c'est un peu juste d'où l'idée d'un cluster.

## une solution temporaire et immédiate

Faire un cluster de boards permet d'avoir un ordinateur libre et puissant et d'ici quelques années j'ai l'espoir que se creer des ordinateurs libres dotés d'une autre architecture. blabla pas le temps blabla

J'ai constaté que la créativité ne se limite pas logiciel, il existe de plus en plus de boards et une volonté de les faire libre. 

qq refs en vrac: [96boars](https://www.96boards.org/), [libre computer board](https://libre.computer/2018/04/08/aml-s905x-cc-mainline-linux-preview-image-8-with-emmc-support/), 

spec d'une architecture de processeur opensource : [riscv](https://riscv.org/)
travail sur l'implementation : [low risc](https://www.lowrisc.org/).
exemple d'implementation : [hi five](https://www.sifive.com/products/hifive-unleashed/)




~~~on peut séparer l'embarquer en trois branches :


### branche électroménager : TV/lave linge

Le monde de l'électroménager est clairement fermé.

### branche smartphone

La branche smartphone est très fermée mais cela semble de moins en moins accepté.
*** article vers [fairphone](), [librem5](),[lineage](), [replicant](), [postmarketos](https://www.postmarketos.org/) ***

### branche développement : {orange,banana,raspberry}pi/hummingboard/librecomputer board

La linux foundation et les actueurs linux ont toujours souhaité garder la main sur le monde de l'embarqué. La philosophie linux c'est de garder le code ouvert et livre en priorité. Ce n'est malheureusement pas un incontournable. 
Le rasperryPi est de plus en plus ouvert : l'[openfirmware](https://github.com/christinaa/rpi-open-firmware) permet de booter un noyau linux . Cela ne supporte pas encore l'affichage sur écran donc pas de quoi sauter au plafond mais c'est un début.

L'orange PI a plusieurs model entierement opensource. le bananaPI je ne sais pas.

L'hummingboard et la libreboard sont 100% opensource.


Contrairement au monde des PC on a parfois dans le monde de l'embarqué la possibilité d'avoir une machine 100% libre. Malheuresement, il n'y a pas la puissance de calcul d'un PC...
On dit souvent que leur puissance est ridicule. Je pense que cela n'est pas fondé. Une de ces board peut décoder la 4K, faire tourner la plupart des programmes web et multimedia et permet de coder. un cat /proc/cpuinfo montre qu'un proc de raspi3B+ fait 38bogomips alors qu'un core1 duo en fait 3324.Même si l'on enleve les capacités graphiques d'un raspberry pi, il n'est pas 100 fois moins puissant qu'un core duo.
J'ai même l'impression que le raspberry pi est parfois plus rapide. Le bogomips ne me semble pas representatif et les SBC sont sous estimés.
~~~
