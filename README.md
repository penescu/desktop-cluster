# Grappe de bureau

Ce projet consiste a creer un cluster à partir de dev board. Rien de nouveau, c'est vrai encore que... La nouveauté est dans l'utilisation. Nous allons partir de matériel et logiciels entierement libre afin de creer un PC entierement libre.
 Cela devient comment faire du nouveau a partir de vielle recette pas du tout originales :)

## D'où vient ce projet?

Depuis quelques mois je cherche un PC entierement libre. Peu m'importe la puissance de l'engin je veux qu'il soit libre, c'est à dire qu'il n'ai aucun secret. Qu'aucun binaire ne soit indispensable à son fonctionnement, pas d'IME*, pas d'AMT*, pas de BIOS complexe et fermé. Un PC simple qui me permette de naviguer sur le web, faire des travaux multimédia, m'interfacer avec des periphériques externes et autres composants type microcontrolleurs par exemple. Enfin je souhaite un peu de puissance de calcul pour la compilation. Si je ne veux pas de binaire il faut que j'ai la puissance necessaire pour compiler mes sources.

Je m'y suis peut être mal pris mais je n'ai pas trouvé. Mon PC actuel est un X60. C'est pas mal mais il commence à trainer la patte. Que faire... C'est là que j'ai eu cette idée. Il existe des boards de dev plus ou moins chère et plus ou moins libre en les assemblants je pourrais obtenir mon PC.

## Comment faire?

Je mettrais les liens plus tard : mais en un mot beowulf.

## Quel matériel?

*) L'ORANGE PI PC(v1) semble un bon candidat pour du lowcost. 

ou

*) La libre computer board : 35$ avec 1GB / 45$ avec 2GB de ram.

ou

*) L'humming board parfait pour ceux qui ont les moyens(entre 100 et 200€ la board mais elle est libre sans bidouille).

## Pourquoi s'obstiner avec les sources et le logiciel libre?

Bonne question, je detaillerais peut etre plus tard mais en quelques mots :

1) Dire non à l'obsolescence programmée.

2) Pouvoir mettre fin au chantage lié aux données personnelles. 

3) Etre capable de m'assurer que le matériels et les logiciels que j'uilise font bien ce qu'ils sont sensé faire.

4) Réduire la fracture technologique en permettant à chacun de s'interesser au fonctionnement de ce qui innonde notre quotidien.

5) Faciliter l'amélioration continue. Chacun peut apporter selon son envie, son interet, ses possibilités.

## BOM : Combien ça coute

Je ne sais pas encore. Si l'on part sur du orange PI PC(v1). C'est environ 17€ / board+cable USB <-> DC. je pense que 4 c'est pas mal pour commencer. Ensuite il multichargeur USB ~22€, un switch ETHERNET ~15€.

* 4 X PI PC(v1) == 4 X 17
* 1 multi chargeur USB == 22
* 1 switch ETH == 15
* ventilation == 15
* des vis??? ~= ??

TOTAL : 120 euro sans les vis... La libre board semble prometteuse mais je ne sais pas ce que cela donne reellement. Tient-elle ses promesses? Elle coute 45 au lieu de 17. On se retrouve grosso modo à 260€ mais avec des proc 64Bit et 2GB de ram par board. ça promet.
